// 1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

function findAllWebDevelopers(data) {
  if (Array.isArray(data)) {
    const webDevelopers = data.filter((item) => {
      if (item.job.toLowerCase().includes("web developer")) {
        return item;
      }
    });
    return webDevelopers;
  } else {
    return [];
  }
}
module.exports = findAllWebDevelopers;
