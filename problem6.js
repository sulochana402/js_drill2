// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

function calculateAverageSalariesByCountry(data) {
  if (Array.isArray(data)) {
    const totalSalariesByCountry = data.reduce((acc, person) => {
      const parsedSalary = parseFloat(person.salary.replace("$", ""));
      acc[person.location] = (acc[person.location] || 0) + parsedSalary;
      return acc;
    }, {});

    const countByCountry = data.reduce((acc, person) => {
      acc[person.location] = (acc[person.location] || 0) + 1;
      return acc;
    }, {});

    const averageSalariesByCountry = {};
    for (const country in totalSalariesByCountry) {
      averageSalariesByCountry[country] =
        totalSalariesByCountry[country] / countByCountry[country];
    }

    return averageSalariesByCountry;
  } else {
    return [];
  }
}

module.exports = calculateAverageSalariesByCountry;
