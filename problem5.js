// 5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function calculateTotalSalariesByCountry(data) {
  if (Array.isArray(data)) {
    const totalSlaries = data.reduce((salariesByCountry, person) => {
      let parsedSalaries = parseFloat(person.salary.replace("$", ""));
      if (!salariesByCountry[person.location]) {
        salariesByCountry[person.location] = 0;
      }
      salariesByCountry[person.location] += parsedSalaries;
      return salariesByCountry;
    }, []);
    return totalSlaries;
  }
}
module.exports = calculateTotalSalariesByCountry;
