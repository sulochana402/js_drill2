// 4. Find the sum of all salaries.

function calculateTotalSalaries(data) {
  if (Array.isArray(data)) {
    return data.reduce((total, item) => {
      let parsedSalary = parseFloat(item.salary.replace("$", ""));
      return total + parsedSalary;
    }, 0);
  } else {
    return [];
  }
}

module.exports = calculateTotalSalaries;
