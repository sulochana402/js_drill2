// 2. Convert all the salary values into proper numbers instead of strings.

function convertSalariesToNumbers(data) {
  if (Array.isArray(data)) {
    const properNumbers = data.map((item) => {
      return parseFloat(item.salary.replace("$", ""));
    });
    return properNumbers;
  } else {
    return [];
  }
}
module.exports = convertSalariesToNumbers;
