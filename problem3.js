// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function getCorrectedSalaries(data) {
  if (Array.isArray(data)) {
    return data.map((item) => {
      let parsedSalary = parseFloat(item.salary.replace("$", ""));
      return parsedSalary * 10000;
    });
  } else {
    return [];
  }
}

module.exports = getCorrectedSalaries;
